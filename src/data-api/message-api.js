const fetchData = async () => {
  try {
    const response = await fetch('https://edikdolynskyi.github.io/react_sources/messages.json');
    return response.json();

  } catch (error) {
    console.log(error);
  }
}

export default fetchData;