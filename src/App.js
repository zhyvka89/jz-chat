import './App.css';
import Chat from './components/Chat/Chat';
// import { useState, useEffect } from "react";
// import fetchData from "./data-api/message-api";
// import Header from "./components/Header/Header";
// import MessageInput from "./components/MessageInput/MessageInput";
// import MessageList from './components/MessageList/MessageList';
// import Preloader from "./components/Preloader/Preloader";

// import styles from './components/Chat/Chat.module.css';

// const Chat = () => {
//   const [data, setData] = useState([]);
//   const [ownData, setOwnData] = useState([]);
//   const allMessages = [...data, ...ownData];
//   console.log(allMessages);

//   useEffect(() => {
//     fetchData().then(result => setData(result));
//   }, []);

//   const handleSubmitInput = (e, message, callback) => {
//     e.preventDefault();
    
//     const date = Date.now();
    
//     const newData = {
//       id: date,
//       user: 'Me',
//       message,
//       createdAt: new Date(date)
//     }

//     console.log(newData);
    
//     setOwnData(prev => [...prev, newData]);
//     callback('');
//   }

//   const onDeleteBtn = (id) => {
//     setOwnData(prev => {
//       return prev.filter(message => message.id !== id);
//     })
//   }

//   return (
//     data.length === 0 ? (
//       <Preloader />
//     ) : (
//       <div className={styles.chat}>
//         <Header data={allMessages} />
//         <MessageList data={data} ownMessages={ownData} onDeleteBtn={onDeleteBtn} />
//         <MessageInput onSubmit={handleSubmitInput} />
//       </div>
//     )
//   )
// }

// export default Chat;

const App = () => {
  return (
    <Chat/>
  )
}

export default App;