import { useEffect, useRef } from 'react';
import Message from '../Message/Message';
import OwnMessage from '../OwnMessage/OwnMessage';

import styles from './MessageList.module.css';

const MessageList = ({ data, ownMessages, onDeleteBtn }) => {
  const scrollElementRef = useRef(null);

  useEffect(() => {
    scrollElementRef.current.scrollIntoView({ behavior: 'smooth' });
  })

  return (
    <div className={styles.chatField}>
      <ul >
        {data.map(item => {
          return (
            <li key={item.id}>
              <Message avatar={item.avatar} message={item.text} name={item.user} time={item.createdAt}/>
            </li>
          )
        })}
        {ownMessages.map(item => {
            return (
              <li key={item.id} className={styles.ownMessage}>
                <OwnMessage data={item} onDeleteBtn={onDeleteBtn}/>
              </li>
            )
          })
        }
        <div ref={scrollElementRef}></div>
      </ul>
    </div>
  )
}

export default MessageList;