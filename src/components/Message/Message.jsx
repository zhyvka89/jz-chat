import { useState } from "react";

import { Avatar, IconButton, Card, CardActions, CardContent, Typography } from "@mui/material";
import ThumbUpIcon from '@mui/icons-material/ThumbUp';

import styles from './Message.module.css';

const Message = ({ avatar, message, name, time }) => {
  const [isClicked, setIsClicked] = useState(false);

  const handleClickLike = () => {
    setIsClicked(prev => !prev);
  }

  const t = new Date(time);
  const year = t.getFullYear();
  const month = t.getMonth() + 1;
  const day = t.getDate();
  const hours = t.getHours();
  const mins = t.getMinutes();

  return (
    <Card
      sx={{
        width: 400,
        boxShadow: '10px 10px 20px 2px rgba(39, 247, 3, 0.29)',
        margin: 4
      }}>
      <CardContent
        sx={{
          display: 'flex',
          alignItems: 'center'
        }}>
        <Avatar
          sx={{
            width: '54px',
            height: '54px'
          }}
          src={avatar}
          alt={name} />
        <Typography
          sx={{
            fontFamily: 'Oooh Baby',
            display: 'flex',
            flexDirection: 'column',
            marginLeft: 2
          }}>
          <span className={styles.messageName}>{name}</span>
          <span>{message}</span>
          <span className={styles.messageTime}>{day}.{month}.{year} {hours}:{mins}</span>
        </Typography>
      </CardContent>
      <CardActions sx={{display: 'flex', justifyContent: 'flex-end'}}>
        <IconButton onClick={handleClickLike}>
          <ThumbUpIcon color={isClicked ? 'primary' : 'inherit'}/>
        </IconButton>
      </CardActions>
    </Card>
  )
}

export default Message;