import { IconButton, Input, Box } from '@mui/material';
import SendIcon from '@mui/icons-material/Send'
import { useState } from 'react';

// import styles from './MessageInput.module.css';

const MessageInput = ({onSubmit}) => {
  const [inputValue, setInputValue] = useState('');

  const onChangeInput = (e) => {
    setInputValue(e.target.value);
  }

  return (
    <Box component='div'
      sx={{
        width: 500,
        border: 'solid 2px yellow',
        padding: 2,
        borderRadius: 2,
        boxShadow: '4px 4px 8px 0px rgba(202, 249, 0, 0.2)',
      }}>
      <form onSubmit={(e) => onSubmit(e, inputValue, setInputValue)}>
        <Input
          sx={{ width: 430, border: 'none' }}
          value={inputValue}
          onChange={onChangeInput}
        />
        <IconButton type='submit' >
          <SendIcon/>
        </IconButton>
      </form>
    </Box>
  )
}

export default MessageInput;