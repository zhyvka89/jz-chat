import styles from './Header.module.css';

const Header = ({ data }) => {
  const names = data.map(item => item.user);
  const unigueNames = Array.from(new Set(names));
  
  return (
    <header className={styles.header}>
      <span className={styles.title}>Funny Bunny</span>
      <span>{unigueNames.length} participants</span>
      <span><b style={{color: 'blue'}}>{data.length}</b> messages</span>
      <span>last message <br /> </span>
    </header>
  )

}

export default Header;