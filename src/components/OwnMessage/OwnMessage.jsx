import { IconButton, Card, CardActions, CardContent, Typography } from "@mui/material";
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';

const OwnMessage = ({data, onDeleteBtn}) => {
  return (
    <Card sx={{width: 400, boxShadow: '-10px -10px 20px 2px rgba(3, 247, 161, 0.2)', margin: 4}}>
      <CardContent>
        <Typography sx={{ fontFamily: 'Oooh Baby', display: 'flex', flexDirection: 'column' }}>
          <span style={{ fontFamily: "Josefin Sans", fontSize: '18px', color: 'blue' }}>{data.user}</span>
          <span>{data.message}</span>
          <span style={{fontFamily: "Josefin Sans", fontSize: '12px'}}>{data.createdAt.hours}:{data.createdAt.mins}</span>
        </Typography>
      </CardContent>
      <CardActions>
        <IconButton>
          <EditIcon/>
        </IconButton>
        <IconButton onClick={() => onDeleteBtn(data.id)}>
          <DeleteIcon/>
        </IconButton>
      </CardActions>
    </Card>
  )
}

export default OwnMessage;