import { Box, CircularProgress } from "@mui/material";

const Preloader = () => {
  return (
    <Box>
      <CircularProgress/>
    </Box>
  )
}

export default Preloader;